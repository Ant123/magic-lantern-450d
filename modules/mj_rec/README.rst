MJPEG video recorder
=========================

Records Motion JPEG AVI video.
In Live View mode press the shutter halfway to start recording.

Modes:

* Simple: press the shutter halfway to start recording.

:Author: Ant123
:License: GPL
:Summary: Records Motion JPEG AVI video.
