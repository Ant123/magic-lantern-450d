/** Motion JPEG Video **/

#include <module.h>
#include <dryos.h>
#include <bmp.h>
#include <menu.h>
#include <config.h>
#include <property.h>
#include <raw.h>
#include <shoot.h>
#include <zebra.h>
#include <beep.h>
#include <lens.h>
#include <focus.h>
#include <string.h>
#include <battery.h>
#include <powersave.h>
#include "../lv_rec/lv_rec.h"
#include "../mlv_rec/mlv.h"
#include <patch.h>

#include "edmac.h"
#include "edmac-memcpy.h"


static uint64_t ret_0_long() { return 0; }

extern WEAK_FUNC(ret_0) void display_filter_get_buffers(uint32_t** src_buf, uint32_t** dst_buf);


static CONFIG_INT( "mj.rec", mj_rec_enabled, 0 );
static CONFIG_INT( "mj.rec.mode", mj_rec_mode, 0 );
#define mj_rec_MODE_NORMAL 0
#define mj_rec_MODE_DEBUG 1

#define LEDBLUE     *(volatile int*)0xC02200E8
#define LEDRED      *(volatile int*)0xC02200E0
#define LEDON   0x46
#define LEDOFF  0x44

static char mjpeg_file_name[100];

static char mjpeg_dbg_buf[255];

// static void * fullsize_buffers[2]; 

static int FrameTime, NewFrameTime, OldFrameTime;
static int FioTime, NewFioTime, OldFioTime;
static int RdyFrameTime, MCpyFrameTime;

// static uint32_t mlv_max_filesize = 0xFFFFFFFF;

static int mj_rec_started, frame_rdy, write_done;

uint32_t lvsize;
uint32_t* para;
char* lvbuf;
char* pFioBuf[2];
char* fiobuf[2];
static int	StartWriteTime;
static int	StopWriteTime;
static int	FrameCount;
static int	max_frame_size;
static int	chunk_size[2];
static int	CurrentBufIndex;
static int	FioBufIndex;
static int	DropCount;
static int	FioBufFull[2];


FILE* save_file;
static volatile int len = 0;
// static void* mjpeg = 0;

// RIFFFILE* mjpeg;


#define FIOBUF_SZ	8*1024*1024		// 8M



#define HEADERBYTES 0x1000			// 4k
#define DEFCACHE_SZ	0x10000		// 1M

#define MAXSOFTDESC_SZ	30
#define MAXCOMMENT_SZ	64
#define MAXDATE_SZ		22

#pragma pack(push, 2)

struct RiffChunk
{
	uint32_t ckID;					// fourcc
	uint32_t ckSize;				// size of chunk data
	unsigned char *ckData;			// chunk data
};

// size - 56
struct AVIHeader
{
	uint32_t dwMicroSecPerFrame;
	uint32_t dwMaxBytesPerSec;		//
	uint32_t dwReserved1;			// must be 0
	uint32_t dwFlags;				// 0 ?
	uint32_t dwTotalFrames;
	uint32_t dwInitialFrames;		// here must be 0
	uint32_t dwStreams;				// number of streams
	uint32_t dwSuggestedBufferSize;
	uint32_t dwWidth;				// width of frame
	uint32_t dwHeight;				// height of frame
	uint32_t dwReserved[4];			// all must be 0
};

// size - 56
struct AVIStreamHeader
{
	uint32_t fccType;				// 'vids'
	uint32_t fccHandler;			// 'mjpg'
	uint32_t dwFlags;				// here 0
	uint16_t wPriority;				// here 0
	uint16_t wLanguage;				// here 0
	uint32_t dwInitialFrames;		// here 0
	uint32_t dwScale;				// dwMicroSecPerFrame
	uint32_t dwRate;				// 1000000
	uint32_t dwStart;				// here 0
	uint32_t dwLength;				// dwTotalFrames
	uint32_t dwSuggestedBufferSize;	//  size largest chunk in the stream
	uint32_t dwQuality;				// from 0 to 10000
	uint32_t dwSampleSize;			// here 0
	struct							// here all field zero
	{
		uint16_t left;
		uint16_t top;
		uint16_t right;
		uint16_t bottom;
	} rcFrame;
};

// size - 40
struct AVIStreamFormat
{
	uint32_t biSize;				// must be 40
	int32_t  biWidth;				// width of frame
	int32_t  biHeight;				// height of frame
	uint16_t biPlanes;				// here must be 1
	uint16_t biBitCount;			// here must be 24
	uint32_t biCompression;			// here 'MJPG' or 0x47504A4D
	uint32_t biSizeImage;			// size, in bytes, of the image (in D90_orig.avi 2764800)
	int32_t  biXPelsPerMeter;		// here 0
	int32_t  biYPelsPerMeter;		// here 0
	uint32_t biClrUsed;				// here 0
	uint32_t biClrImportant;		// here 0
};

struct AVIIndexChunk
{
	uint32_t ckID;
	uint32_t flags;					// unknown?
	uint32_t offset;				// offset from 'movi' to video frame chunk
	uint32_t size;					// size of video frame chunk
};

// #pragma pack(pop)

typedef struct
{
	// int fd;							// file descriptor
	FILE* fd;							// file descriptor
	uint32_t realHeaderSize;
	uint32_t frames;
	double fps;
	unsigned char* index;
	uint32_t index_curpos;
	uint32_t* pindex_real_size;
	uint32_t index_size;
	unsigned char* header;
	uint32_t *pFileSize;
	uint32_t *pDataSize;
	struct AVIHeader* aviheader;
	struct AVIStreamHeader* avistreamheader;
	struct AVIStreamFormat* avistreamformat;
	char* pSoftStr;
	char* pCommentStr;
	char* pDateStr;
	unsigned char* cache;
	unsigned int cache_sz;
	unsigned int cache_pos;
} RIFFFILE;


static RIFFFILE* mjpeg;

void* mjpegCreateFile(const char* fname)
{
	// int fd = open(fname, O_BINARY | O_WRONLY | O_CREAT | O_TRUNC, 0644);
	// FILE* fd = FIO_OpenFile(fname, O_RDWR | O_SYNC);
	FILE* fd = FIO_CreateFile(fname);
	// if (fd < 0)
	if (!fd)
		return 0;

	RIFFFILE* riff = (RIFFFILE*)malloc(sizeof(RIFFFILE));
	if (!riff)
	{
		FIO_CloseFile(fd);
		return 0;
	}
	memset(riff, 0, sizeof(RIFFFILE));
	riff->fd = fd;
	riff->header = (unsigned char*)malloc(HEADERBYTES);
	if (!riff->header)
	{
		FIO_CloseFile(riff->fd);
		free(riff);
		return 0;
	}
	memset(riff->header, 0, HEADERBYTES);

	riff->index_size = 0x100;
	riff->index = (unsigned char*)malloc(riff->index_size);
	if (!riff->index)
	{
		FIO_CloseFile(riff->fd);
		free(riff->header);
		free(riff);
		return 0;
	}

	riff->pFileSize = (uint32_t*)(riff->header + 4);
	*riff->pFileSize = HEADERBYTES - 8;
	uint32_t* pnum = 0;
	uint32_t offset = 0;
	char* ptr = (char*)riff->header;					// addr = 0
	strncpy(ptr, "RIFF", 4);
	offset += 8;
	ptr = (char*)(riff->header + offset);				// addr = 8
	strncpy(ptr, "AVI LIST", 8);
	offset += 8;
	pnum = (uint32_t*)(riff->header + offset);			// addr = 16
	*pnum = 40 + sizeof(struct AVIHeader) + sizeof(struct AVIStreamHeader) + sizeof(struct AVIStreamFormat);
	offset += 4;
	ptr = (char*)(riff->header + offset);				// addr = 20
	strncpy(ptr, "hdrlavih", 8);
	offset += 8;
	pnum = (uint32_t*)(riff->header + offset);			// addr = 28
	*pnum = sizeof(struct AVIHeader);
	offset += 4;
	riff->aviheader = (struct AVIHeader*)(riff->header + offset);	// addr = 32
	riff->aviheader->dwStreams = 1;						// only video stream
	riff->aviheader->dwFlags = 0x110;					// has index & interlieved
	offset += sizeof(struct AVIHeader);
	ptr = (char*)(riff->header + offset);				// addr = 88
	strncpy(ptr, "LIST", 4);
	offset += 4;
	pnum = (uint32_t*)(riff->header + offset);			// addr = 92
	*pnum = 20 + sizeof(struct AVIStreamHeader) + sizeof(struct AVIStreamFormat);
	offset += 4;
	ptr = (char*)(riff->header + offset);				// addr = 96
	strncpy(ptr, "strlstrh", 8);
	offset += 8;
	pnum = (uint32_t*)(riff->header + offset);			// addr = 104
	*pnum = sizeof(struct AVIStreamHeader);
	offset += 4;
	riff->avistreamheader = (struct AVIStreamHeader*)(riff->header + offset);	// addr = 108
	riff->avistreamheader->fccType = 0x73646976;		// 'vids'
	riff->avistreamheader->fccHandler = 0x67706a6d;		// 'mjpg'
	offset += sizeof(struct AVIStreamHeader);
	ptr = (char*)(riff->header + offset);				// addr = 164
	strncpy(ptr, "strf", 4);
	offset += 4;
	pnum = (uint32_t*)(riff->header + offset);			// addr = 168
	*pnum = sizeof(struct AVIStreamFormat);
	offset += 4;
	riff->avistreamformat = (struct AVIStreamFormat*)(riff->header + offset);	// addr = 172
	offset += sizeof(struct AVIStreamFormat);

	ptr = (char*)(riff->header + offset);				// addr = 212
	strncpy(ptr, "LIST", 4);
	offset += 4;
	pnum = (uint32_t*)(riff->header + offset);			// addr = 216
	*pnum = 12 + MAXSOFTDESC_SZ + 8 + MAXCOMMENT_SZ + 8 + MAXDATE_SZ;	// ISFT, ICMT & ICRD
	offset += 4;
	ptr = (char*)(riff->header + offset);				// addr = 220
	strncpy(ptr, "INFOISFT", 8);
	offset += 8;
	pnum = (uint32_t*)(riff->header + offset);			// addr = 228
	*pnum = MAXSOFTDESC_SZ;								// ISFT
	offset += 4;
	riff->pSoftStr = (char*)(riff->header + offset);	// addr = 232
	// fill this later
	offset += MAXSOFTDESC_SZ;
	ptr = (char*)(riff->header + offset);				// addr = 262
	strncpy(ptr, "ICMT", 4);
	offset += 4;
	pnum = (uint32_t*)(riff->header + offset);			// addr = 266
	*pnum = MAXCOMMENT_SZ;								// ICMT
	offset += 4;
	riff->pCommentStr = (char*)(riff->header + offset);	// addr = 270
	// fill this later
	offset += MAXCOMMENT_SZ;

	ptr = (char*)(riff->header + offset);				// addr = 334
	strncpy(ptr, "ICRD", 4);
	offset += 4;
	pnum = (uint32_t*)(riff->header + offset);			// addr = 338
	*pnum = MAXDATE_SZ;									// ICRD
	offset += 4;
	riff->pDateStr = (char*)(riff->header + offset);	// addr = 342
	// fill this later
	offset += MAXDATE_SZ;

	riff->realHeaderSize = offset;

	// JUNK chunk
	ptr = (char*)(riff->header + offset);				// addr = 364
	strncpy(ptr, "JUNK", 4);
	offset += 4;
	uint32_t junk_size = HEADERBYTES - riff->realHeaderSize - 20;
	pnum = (uint32_t*)(riff->header + offset);			// addr = 368
	*pnum = junk_size;
	offset += 4;

	ptr = (char*)(riff->header + HEADERBYTES - 12);		// addr = 4084
	strncpy(ptr, "LIST", 4);
	riff->pDataSize = (uint32_t*)(riff->header + HEADERBYTES - 8);	// 4088
	*riff->pDataSize = 4;
	ptr += 8;											// addr = 4092
	strncpy(ptr, "movi", 4);

	// for index chunk
	riff->pindex_real_size = (uint32_t*)(riff->index + 4);
	ptr = (char*)riff->index;
	strncpy(ptr, "idx1", 4);
	*riff->pindex_real_size = 0;
	riff->index_curpos = 8;
	*riff->pFileSize += 8;

	// riff->cache_sz = DEFCACHE_SZ;
	// riff->cache_pos = 0;
	// riff->cache = (unsigned char*)malloc(riff->cache_sz);
	// if (!riff->cache)
	// {
		// FIO_CloseFile(riff->fd);
		// free(riff->header);
		// free(riff->index);
		// free(riff);
		// return 0;
	// }

	if (!FIO_WriteFile(riff->fd, riff->header, HEADERBYTES))
	{
		FIO_CloseFile(riff->fd);
		free(riff->header);
		free(riff->index);
		free(riff);
		return 0;
	}

	return (void*)riff;
}

int mjpegSetup(void* p, int fwidth, int fheight, double fps, int quality)
{
	RIFFFILE* rf = (RIFFFILE*)p;
	if (!rf)
		return 0;
	rf->fps = fps;
	//memset(rf->aviheader, 0, sizeof(struct AVIHeader));
	rf->aviheader->dwMicroSecPerFrame = (uint32_t)(1000000.0/fps);
	rf->aviheader->dwWidth = fwidth;
	rf->aviheader->dwHeight = fheight;
	//memset(rf->avistreamheader, 0, sizeof(struct AVIStreamHeader));
	rf->avistreamheader->dwScale = 1000;
	rf->avistreamheader->dwRate = (uint32_t)(1000.0*fps);
	rf->avistreamheader->dwQuality = quality;
	//memset(rf->avistreamformat, 0, sizeof(struct AVIStreamFormat));
	rf->avistreamformat->biSize = 40;
	rf->avistreamformat->biWidth = fwidth;
	rf->avistreamformat->biHeight = fheight;
	rf->avistreamformat->biPlanes = 1;
	rf->avistreamformat->biBitCount = 24;
	rf->avistreamformat->biCompression = 0x47504A4D;			// 'MJPG'
	rf->avistreamformat->biSizeImage = fwidth*fheight*3;
	return 1;
}

int mjpegSetInfo(void* p, const char* software, const char* comment, const char* date)
{
	RIFFFILE* rf = (RIFFFILE*)p;
	if (!rf)
		return 0;
	if (software)
	{
		strncpy(rf->pSoftStr, software, MAXSOFTDESC_SZ - 1);
		rf->pSoftStr[MAXSOFTDESC_SZ - 1] = 0;
	}
	else
		rf->pSoftStr[0] = 0;
	if (comment)
	{
		strncpy(rf->pCommentStr, comment, MAXCOMMENT_SZ - 1);
		rf->pCommentStr[MAXCOMMENT_SZ - 1] = 0;
	}
	else
		rf->pCommentStr[0] = 0;
	if (date)
	{
		strncpy(rf->pDateStr, date, MAXDATE_SZ - 1);
		rf->pDateStr[MAXDATE_SZ - 1] = 0;
	}
	else
		rf->pDateStr[0] = 0;
	return 1;
}


int mjpegSetMaxChunkSize(void* p, unsigned int sz)
{
	RIFFFILE* rf = (RIFFFILE*)p;
	if (!rf)
		return 0;
	rf->aviheader->dwSuggestedBufferSize = sz;
	rf->aviheader->dwMaxBytesPerSec = (int)(sz*rf->fps) + 1;
	rf->avistreamheader->dwSuggestedBufferSize = sz;
	return 1;
}


int mjpegCloseFile(void* p)
{
	RIFFFILE* rf = (RIFFFILE*)p;
	if (!rf)
		return 0;

	int res = 1;
	// write index
	if (rf->index)
	{
		// if (cached_write(rf, rf->index, *rf->pindex_real_size + 8) != *rf->pindex_real_size + 8)
		if (FIO_WriteFile(rf->fd, rf->index, *rf->pindex_real_size + 8) != *rf->pindex_real_size + 8)
			res = 0;
	}
	else
		res = 0;
	// cache_flush(rf);
	// msleep(1500);
	// write modified header
	rf->aviheader->dwTotalFrames = rf->frames;
	rf->avistreamheader->dwLength = rf->frames;
	FIO_SeekSkipFile(rf->fd, 0, SEEK_SET);
	// FIO_CloseFile(rf->fd);
	// rf->fd = FIO_OpenFile("dm.log", O_RDWR | O_SYNC);
	
	
	if (FIO_WriteFile(rf->fd, rf->header, HEADERBYTES) != HEADERBYTES)
		res = 0;
	else
		LEDBLUE = LEDOFF;
	FIO_CloseFile(rf->fd);
	free(rf->header);
	if (rf->index)
		free(rf->index);
	// if (rf->cache)
		// free(rf->cache);
	free(rf);
	return res;
}

static void mjpeg_video_rec_task()
{
int	LocalBufIndex;
while(1)
{
	// if (mj_rec_started && frame_rdy && chunk_size[FioBufIndex] && FioBufFull[FioBufIndex])
	if (mj_rec_started && FioBufFull[FioBufIndex])
	// if (frame_rdy)
	{
		LocalBufIndex = FioBufIndex;
		write_done = 0;
		// FrameTime = get_ms_clock_value() - OldFrameTime;
		OldFioTime = get_ms_clock_value();
		
		pFioBuf[LocalBufIndex] = fiobuf[LocalBufIndex];
		RIFFFILE* rf = (RIFFFILE*)mjpeg;
		FIO_WriteFile(rf->fd, pFioBuf[LocalBufIndex], chunk_size[LocalBufIndex]);
		
		len += chunk_size[LocalBufIndex];
		*rf->pDataSize += chunk_size[LocalBufIndex];
		*rf->pFileSize += chunk_size[LocalBufIndex];// + sizeof(struct AVIIndexChunk);
		// rf->frames++;

		chunk_size[LocalBufIndex] = 0;
		pFioBuf[LocalBufIndex] = fiobuf[LocalBufIndex];
		FioBufFull[LocalBufIndex] = 0;
		frame_rdy = 0;	
		
		FioTime = get_ms_clock_value() - OldFioTime;
		// OldFrameTime = get_ms_clock_value();
		write_done = 1;
	}	
	else
		msleep(1);
}

}

static void process_frame()
{
	RIFFFILE* rf = (RIFFFILE*)mjpeg;
	uint32_t* pnum;
	char buff[9];
	
	if ((chunk_size[CurrentBufIndex] + ((lvsize+0x7FF) & ~0x7FF) + 9) > FIOBUF_SZ)
	{
		FioBufFull[CurrentBufIndex] = 1;
		FioBufIndex = CurrentBufIndex;
		CurrentBufIndex ^= 1;
		frame_rdy = 1;
	}
	if (!FioBufFull[CurrentBufIndex])
	{
		// if ((chunk_size[0] == 0) || (chunk_size[1] == 0) || ! write_done)	
		// {	
		pnum = (uint32_t*)(buff);
		*pnum = (uint32_t)(0x63643030); // "00dc"
		pnum = (uint32_t*)(buff + 4);
		*pnum = (uint32_t)lvsize;
		memcpy(pFioBuf[CurrentBufIndex], buff, 8);
		pFioBuf[CurrentBufIndex] += 8;
		edmac_memcpy(pFioBuf[CurrentBufIndex], lvbuf, ((lvsize+0x7FF) & ~0x7FF));
		pFioBuf[CurrentBufIndex] += ((lvsize+1) & ~0x1);
		chunk_size[CurrentBufIndex] += ((lvsize+1) & ~0x1) + 8;
		if (max_frame_size < lvsize)
				max_frame_size = lvsize;

		// len += chunk_size[0];
		// *rf->pDataSize += chunk_size[0];
		// *rf->pFileSize += chunk_size[0];// + sizeof(struct AVIIndexChunk);
		rf->frames++;		
		// FrameCount++;
		// }
	}
	else
	{
		// frame_rdy = 0;
		DropCount++;
	}
	
	
}

#if 0
int (*LVJpegEncodePath)() = 0xFFBF9F44;
// int (*LVJpegEncodePath)() = 0xFFBF9DF8; //0

int LVJpegEncodePath_my(int* param1)
{
	uint32_t* params = param1;	
	para = param1;
	DryosDebugMsg(0, 3, "[ML-MJPEG] LVJpegEncodePath %08x", param1);
	DryosDebugMsg(0, 3, "[ML-MJPEG] Values 0x00: %08x %08x %08x %08x", *params,*(params+1),*(params+2),*(params+3) );
	DryosDebugMsg(0, 3, "[ML-MJPEG] Values 0x10: %08x %08x %08x %08x", *(params+4),*(params+5),*(params+6),*(params+7) );
	
	return LVJpegEncodePath(param1);

}
#endif

static void mjpeg_frame_rdy(int class, int level, char* fmt, ...)
{
   
	if (!mj_rec_started) return;
  
	lvsize = MEM(0x8304);
	lvbuf = MEM(0x8308);
	
	if (FrameCount)
	{
	NewFrameTime = get_us_clock_value();
	FrameTime = NewFrameTime - OldFrameTime;
	OldFrameTime = NewFrameTime;
	}
	
	RdyFrameTime = get_us_clock_value();
	DryosDebugMsg(0, 3, "[ML-MJPEG] start edmac_memcpy at %08x frame#%05d, %06d bytes", lvbuf, FrameCount, lvsize );
	process_frame();
	MCpyFrameTime = get_us_clock_value() - RdyFrameTime;

	FrameCount++;
    return;
		

}


static MENU_UPDATE_FUNC(mj_rec_display)
{
    /* reset the MLV frame counter if we enter ML menu */
    /* exception: don't reset if the intervalometer is running - we might want to change some settings on the fly */
    
    if (!mj_rec_enabled)
        return;

    switch (mj_rec_mode)
    {
        case mj_rec_MODE_NORMAL:
            MENU_SET_VALUE("Normal");
            break;
        case mj_rec_MODE_DEBUG:
            MENU_SET_VALUE("Debug");
            break;			
    }
}


static char* mj_rec_get_name()
{
    char *extension;
    
    extension = "AVI";
    
    // int file_number = get_shooting_card()->file_number;

        char pattern[100];
        // snprintf(pattern, sizeof(pattern), "%s/%%08d.%s", get_dcim_dir(), 0, extension);
        snprintf(pattern, sizeof(pattern), "B:/DCIM/%%08d.%s", 0, extension);
        get_numbered_file_name(pattern, 99999999, mjpeg_file_name, sizeof(mjpeg_file_name));

    bmp_printf(FONT_MED, 0, 37, "%s    ", mjpeg_file_name);
    return mjpeg_file_name;
}




/* FA test image code only looks at these 3 properties - doesn't know about auto ISO & stuff */
/* lens_info data may not be in sync, e.g. when using expo override */
/* so, check these props directly before taking a picture */
static PROP_INT(PROP_ISO, prop_iso);
static PROP_INT(PROP_SHUTTER, prop_shutter);


static unsigned int mj_rec_polling_cbr(unsigned int ctx)
{
    if (!mj_rec_enabled)
        return 0;

    static int mj_rec_countdown;
    if (!display_idle())
    {
        mj_rec_countdown = 5;
    }
    else if (!get_halfshutter_pressed())
    {
        if (mj_rec_countdown)
            mj_rec_countdown--;
    }

    if (lv && get_halfshutter_pressed() && !mj_rec_started)
    {
        /* half-shutter was pressed while in playback mode, for example */
        if (mj_rec_countdown)
        {
            /* in this case, require a long press to trigger a new picture */
            for (int i = 0; i < 10; i++)
            {
                msleep(50);
                if (!get_halfshutter_pressed())
                {
                    return 0;
                }
            }
        }

        if (lv && !is_manual_focus() && !mj_rec_started)
        {
            /* try to ignore the AF button, and only take pictures on plain half-shutter */
            /* problem: lv_focus_status is not updated right away :( */
            bmp_printf(FONT_MED, 0, 37, "Hold on...");
            for (int i = 0; i < 10; i++)
            {
                wait_lv_frames(1);
                
                if (lv_focus_status == 3)
                {
                    while (get_halfshutter_pressed())
                    {
                        bmp_printf(FONT_MED, 0, 37, "Focusing...");
                        msleep(10);
                    }
                    redraw();
                    return 0;
                }
            }
        }
        if (!fiobuf[0] || !fiobuf[1])
		{
		bmp_printf(FONT_LARGE, 0, 100, "Error: malloc");	
		return 0;
		}
        // mj_rec_take(1);
        char* filename = mj_rec_get_name();		
		// save_file = FIO_CreateFile(filename);
		// save_file = FIO_CreateFile("dm.log");
        msleep(250);
		LEDBLUE = LEDON;
		if (!mj_rec_mode)
		{
			mjpeg = mjpegCreateFile(filename);
			NotifyBox(2000, "Filename:%s", filename);
		}
		else
			mjpeg = mjpegCreateFile("MJREC.AVI");
		
		mjpegSetup(mjpeg, 0, 0, (double)(15.0), 10000);
		// mjpegSetCache(mjpeg, 1024 *1024);		
		pFioBuf[0] = fiobuf[0];
		pFioBuf[1] = fiobuf[1];
		FrameCount = 0;
		DropCount = 0;
		len = 0;
		chunk_size[0] = 0;
		chunk_size[1] = 0;
		FioBufFull[0] = 0;
		FioBufFull[1] = 0;
		write_done = 1;
		mj_rec_started = 1;
		StartWriteTime = get_ms_clock_value();
    }
    
    if (mj_rec_started && !lv)
    {
	mj_rec_started = 0;
	// LEDBLUE = LEDOFF;
		StopWriteTime = get_ms_clock_value();
		 while (!write_done)
			msleep(50);
		
		double fps = ((double)FrameCount*1000.0)/(double)(StopWriteTime - StartWriteTime);
		// double fps = (double)(15.0);
		
		mjpegSetup(mjpeg, 848, 560, fps, 10000);
		mjpegSetMaxChunkSize(mjpeg, max_frame_size);
		// mjpegSetInfo(mjpeg, "Magic Lantern", "Motion JPEG Movie recorder module", "2019-01-01 12:00:00");		
		mjpegSetInfo(mjpeg, "Magic Lantern", "Motion JPEG Movie recorder module", __module_string_f_value);		
		mjpegCloseFile(mjpeg);
        int speed = (int)(fps * 10);
		NotifyBox(1000, "MJ-rec: saved %d frames. %02d.%0dfps", FrameCount, speed/10, speed%10);	
	}
	if (mj_rec_started && mj_rec_mode)
	{
		RIFFFILE* rf = (RIFFFILE*)mjpeg;
		bmp_printf(FONT_LARGE, 0, 60, "Fr:%05dus, Cp:%05dus, Fio:%04dms", FrameTime, MCpyFrameTime, FioTime);
		bmp_printf(FONT_LARGE, 0, 100, "Wr:%05d, Drop:%05d, LvSz:%06d", FrameCount, DropCount, lvsize);
		bmp_printf(FONT_LARGE, 0, 140, "chunk0:%08d, chunk1:%08d", chunk_size[0],  chunk_size[1]);
		// bmp_printf(FONT_LARGE, 0, 180, "para:%08x, lvbuf:%08x, para1:%08x",para,  lvbuf, *(para+1));
		// bmp_printf(FONT_LARGE, 0, 140, "Ds:%08d, Fs:%08d", *rf->pDataSize,  *rf->pFileSize);
	}                                                                 
    return 0;
}

static struct menu_entry mjrec_menu[] = {
    {
        .name = "MJPEG Video",
        .priv = &mj_rec_enabled,
        .update = mj_rec_display,
        .max  = 1,
        .depends_on = DEP_LIVEVIEW | DEP_CFN_AF_BACK_BUTTON,
        .help  = "Record Motion JPEG Video in AVI format",
		.submenu_width = 650,
        .children =  (struct menu_entry[]) {
            {
                .name = "MJ Rec Mode",
                .priv = &mj_rec_mode,
                .max = 1,
                .help = "Choose the mode:",
                .choices = CHOICES(
                    "Normal",
                    "Debug"
                ),
                .help2 = 
                    "Normal mode.\n"
                    "Display debug info while recording.\n",
                .icon_type = IT_DICE,
            },
		MENU_EOL,
        }
    },
};


static unsigned int mjrec_init()
{
    /* fixme in core: prop handlers should trigger when initializing, but they do not */
    prop_iso = lens_info.raw_iso;
    prop_shutter = lens_info.raw_shutter;

    menu_add("Movie", mjrec_menu, COUNT(mjrec_menu));

    // uint32_t DebugMsg_addr = (uint32_t)&DryosDebugMsg;	
    uint32_t DebugMsg_addr = (uint32_t)0xFFAD586C;	
        patch_instruction(
            DebugMsg_addr,                              /* hook on the first instruction in DebugMsg */
            MEM(DebugMsg_addr),                         /* do not do any checks; on 5D2 it would be e92d000f, not sure if portable */
            // B_INSTR(DebugMsg_addr, mjpeg_frame_rdy),        /* replace all calls to DebugMsg with our own function (no need to call the original) */
            BL_INSTR(DebugMsg_addr, mjpeg_frame_rdy),        /* replace all calls to DebugMsg with our own function (no need to call the original) */
            "mj-rec: hook DebugMsg calls at 0xFFAD586C"
        );	
#if 0		
		uint32_t LVJpegEncodePath_addr = (uint32_t)0xFFAD5610;
		// uint32_t LVJpegEncodePath_addr = (uint32_t)0xFFAD561C; //0
        patch_instruction(
            LVJpegEncodePath_addr,                              /* hook on the first instruction in DebugMsg */
            MEM(LVJpegEncodePath_addr),                         /* do not do any checks; on 5D2 it would be e92d000f, not sure if portable */
            BL_INSTR(LVJpegEncodePath_addr, LVJpegEncodePath_my),        /* replace all calls to DebugMsg with our own function (no need to call the original) */
            // BL_INSTR(DebugMsg_addr, mjpeg_frame_rdy),        /* replace all calls to DebugMsg with our own function (no need to call the original) */
            "mj-rec: hook LVJpegEncodePath calls at 0xFFAD5610"
        );		
#endif		
	fiobuf[0] = fio_malloc(FIOBUF_SZ);
	fiobuf[1] = fio_malloc(FIOBUF_SZ);
	// memset(fiobuf, 0x55, 1024*1024);
	task_create("MJpeg_Rec_task", 0x19, 0x2000, mjpeg_video_rec_task, (void*)0);
		
    return 0;
}

static unsigned int mjrec_deinit()
{
    fio_free(fiobuf);
	return 0;
}

MODULE_INFO_START()
    MODULE_INIT(mjrec_init)
    MODULE_DEINIT(mjrec_deinit)
MODULE_INFO_END()

MODULE_CBRS_START()
    // MODULE_CBR(CBR_CUSTOM_PICTURE_TAKING, mj_rec_take, 0)
    MODULE_CBR(CBR_SHOOT_TASK, mj_rec_polling_cbr, 0)
    // MODULE_CBR(CBR_VSYNC, mj_rec_raw_vsync, 0)
    // MODULE_CBR(CBR_DISPLAY_FILTER, mj_rec_preview, 0)
MODULE_CBRS_END()

MODULE_CONFIGS_START()
    MODULE_CONFIG(mj_rec_enabled)
    MODULE_CONFIG(mj_rec_mode)
    // MODULE_CONFIG(mj_rec_slitscan_mode)
    // MODULE_CONFIG(mj_rec_file_format)
MODULE_CONFIGS_END()

MODULE_PROPHANDLERS_START()
    MODULE_PROPHANDLER(PROP_ISO)
    MODULE_PROPHANDLER(PROP_SHUTTER)
MODULE_PROPHANDLERS_END()
