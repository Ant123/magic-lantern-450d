// Small feature set, we'll just define each one 
// look in all_features.h

#define CONFIG_RAW_LIVEVIEW

/** Overlay menu **/
#define FEATURE_GLOBAL_DRAW
//~ #define FEATURE_ZEBRA
//~ #define FEATURE_ZEBRA_FAST
//~ #define FEATURE_FOCUS_PEAK
//~ #define FEATURE_FOCUS_PEAK_DISP_FILTER // too slow
//~ #define FEATURE_MAGIC_ZOOM
//~ #define FEATURE_CROPMARKS
//~ #define FEATURE_GHOST_IMAGE
#define FEATURE_SPOTMETER
#define FEATURE_COLOR_SCHEME                // 
//~ #define FEATURE_FALSE_COLOR
#define FEATURE_HISTOGRAM
// #define FEATURE_WAVEFORM
#define FEATURE_VECTORSCOPE

// #define FEATURE_EXPSIM                      // ok but feature not completed

/* Expo menu */
#define FEATURE_EXPO_ISO_DIGIC              //
#define FEATURE_EXPO_LOCK                   //
#define FEATURE_EXPO_ISO                    //
#define FEATURE_EXPO_SHUTTER                //
#define FEATURE_EXPO_APERTURE               //
#define FEATURE_EXPO_PRESET                 //
#define FEATURE_EXPO_OVERRIDE               //
#define FEATURE_PICSTYLE                    //
#define FEATURE_REC_PICSTYLE                //
#define FEATURE_WHITE_BALANCE               //

    
// #define FEATURE_OVERLAYS_IN_PLAYBACK_MODE

#define FEATURE_DONT_CLICK_ME
#define FEATURE_SCREEN_LAYOUT               // 
    
/** Focus menu **/

#define FEATURE_AF_PATTERNS

/** Shoot menu **/

#define FEATURE_INTERVALOMETER
#define FEATURE_LV_DISPLAY_GAIN
#undef FEATURE_FLEXINFO

// #undef CONFIG_RAW_LIVEVIEW
// #define CONFIG_RAW_LIVEVIEW
// #undef FEATURE_OVERLAYS_IN_PLAYBACK_MODE

// anything else working? no idea
#define FEATURE_SCREENSHOT
#define FEATURE_SHOW_GUI_EVENTS
#define FEATURE_SHOW_TASKS
#define FEATURE_SHOW_EDMAC_INFO
#define FEATURE_SHOW_IMAGE_BUFFERS_INFO

#define FEATURE_SHOW_CMOS_TEMPERATURE
#define FEATURE_SHOW_FREE_MEMORY